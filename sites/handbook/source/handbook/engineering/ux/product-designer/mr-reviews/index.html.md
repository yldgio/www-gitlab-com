---
layout: handbook-page-toc
title: Merge Request Reviews
description: "Guidelines for Product Designers when reviewing merge requests."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This page outlines the guidelines for Product Designers when reviewing merge requests (MRs), also called “UX reviews” or “Product Design MR reviews”.

## Requirement

**Almost all merge requests (MRs) change the UX in one way or another, but Product Designers are only required to review and approve MRs that include _user-facing changes_**. Per the [approval guidelines](https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines), “user-facing changes include both visual changes (regardless of how minor) and changes to the rendered DOM that impact how a screen reader may announce the content.”

MRs with only backend changes sometimes affect the UX (for example, performance changes, sorting of lists, etc.), but you _are not required_ to review them unless they fall under the definition of “user-facing changes”.

To help triage, be aware of all MRs in your stage group and ask engineers about which MRs could affect the UX and how. Product Designers often give constructive feedback on any kind of MR, including MRs that _seem_ to not affect the UX, so use your best judgment when deciding which MRs you should review.

If you're asked to review an MR from another stage group and the corresponding Product Designer hasn't been involved yet, remind the author about that Product Designer and request a review from them.

## Workload and response times

MR review requests are the [number one priority for Product Designers](/handbook/engineering/ux/product-designer/#priorities). Respond to them per our [first-response Service-Level Objective](/handbook/engineering/workflow/code-review/#first-response-slo), not only when the request comes from within your stage group, but also when it's a community contribution or another stage group asks for your input.

Balancing MR reviews with other priorities is a challenge, as they can be unpredictable. To avoid disruptions and context-switching, we suggest you block some time every day to review MRs (for example, 30 minutes or 1 hour per day).

If you're struggling with MR reviews, remember to [manage expectations with MR authors](/handbook/engineering/workflow/code-review/#managing-expectation) and let your manager know right away so they can help you.

## Reviewing

In general, follow the [Code Review guidelines](https://docs.gitlab.com/ee/development/code_review.html) (it's a long document, but please read all of it). Exceptions to those guidelines are noted below.

### Understand the MR

- Check the MR description for the following information. If any are missing, ask the author to complete the description before investing time in the review.
    - A thorough explanation of the changes.
    - How you can test the changes.
    - Link(s) to the related issue(s).
    - _Before_ and _After_ screenshots/videos (if appropriate).
- To reduce waiting time, if you need to [preview the MR in a live environment](#preview-the-mr) (like Gitpod, your local GDK, or even a review app), start the environment and let it load in the background while you familiarize yourself with the MR and related issue(s).

### Preview the MR

Always try to preview the MR in a live environment, so that you can experience the changes are users will and review them thoroughly. That said, it's okay to rely on screenshots/videos if they allow you to review everything in the [design and UI changes checklist](https://docs.gitlab.com/ee/development/contributing/design.html#checklist) (usually the case for tiny MRs).

To choose the most appropriate method to preview the MR and get started, see [Preview, test, and contribute](/handbook/engineering/ux/how-we-work/#preview-test-and-contribute). If you get stuck, don't struggle on your own, check our [help section](/handbook/engineering/ux/how-we-work/#help).

### Review the MR

- Follow our review best practices for [everyone](https://docs.gitlab.com/ee/development/code_review.html#everyone) and [reviewers](https://docs.gitlab.com/ee/development/code_review.html#reviewing-a-merge-request).
- Always try to find something worth praising the author for, like a thorough MR description or their attention to detail on a certain aspect. But don't make empty praises, only praise them if you recognize the value of what they've done.
- Separate each topic into its comment thread so that they can be discussed and resolved separately. If possible, create threads on the line(s) of code related to the topic.
- Ensure the author is clear on what is required from them to address/resolve the suggestion.
    - Consider using the [Conventional Comment format](https://conventionalcomments.org/#format) to convey your intent.
    - For non-mandatory suggestions, decorate with (non-blocking) so the author knows they can optionally resolve within the merge request or follow-up at a later stage.
    - There’s a [Chrome/Firefox add-on](https://gitlab.com/conventionalcomments/conventional-comments-button) which you can use to apply [Conventional Comment](https://conventionalcomments.org/) prefixes.
- Review using the [design and UI changes checklist](https://docs.gitlab.com/ee/development/contributing/design.html#checklist) to make sure all main aspects are covered.
   - The team can decide to merge before a full review if the changes remain behind a feature flag and there's a plan for you to do a full review in staging. Beware that this approach can result in unplanned issues and MRs if things need to be fixed.
- Stick to the UX requirements in the issue. See the [follow-ups checklist](https://docs.gitlab.com/ee/development/contributing/design.html#follow-ups) on creating issues for further updates or missing pieces.

### Handoff the MR

- After each review round, you should remove yourself as a reviewer and post a summary comment, letting the author know if changes are required following your review.
- If the MR has [bugs of severity 1 or 2](/handbook/engineering/quality/issue-triage/#severity), you should only approve it when those bugs are fixed. If the MR only has bugs of severity 3 and 4, you can approve it, but we encourage authors to fix them before merging. To address any outstanding UX bugs, you should create follow-up issues and label them as `UX debt` (learn more about this label in [how we use labels](/handbook/engineering/ux/ux-department-workflow/#how-we-use-labels)).
- When you're confident that the MR meets all requirements, to handoff the MR, see the [responsibility of the reviewer](https://docs.gitlab.com/ee/development/code_review.html#the-responsibility-of-the-reviewer).

## Performance indicator

The [Product Design merge request (MR) review volume](/handbook/engineering/ux/performance-indicators/#product-design-mr-review-volume) is tracked as a Key Performance Indicator (KPI) of the UX department. 
