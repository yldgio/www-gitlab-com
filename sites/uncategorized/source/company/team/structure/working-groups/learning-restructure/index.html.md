---
layout: markdown_page
title: "Learning Restructure Working Group"
description: "The GitLab Learning Restructure Working Group aims to create a plan and processes for ensuring a high quality learning experience for users "
canonical_path: "/company/team/structure/working-groups/learning-restructure/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | 2021-09-20|
| Target End Date | 2022-01-31 |
| Slack           | [#wg-learning restructure](https://join.slack.com/share/zt-whs6k6s0-RFQOqiPuY0tKYDnWKcmwdg) (only accessible from within the company) |
| Google Doc      | [Learning Restructure Working Group Agenda](https://docs.google.com/document/d/1P-TEi3yXtRum2pV8KDHON4EIn-mYoH72YpDMJGClFR4/edit#heading=h.3f77vfk6c3o1) (only accessible from within the company) |
| Related Epics and Issues | TBD         |

## Business Goal

1. Fix key current pain points in trainings. Specifically:
   1. Inefficiency
      1. Lack of clarity on training ownership
      1. Duplication in training content
   1. A sub-optimal user experience
      1. Need greater clarity on which part of the training is for what and who
1. Determine ongoing plan, processes, and ownership for future training coordination

## Exit Criteria

### Update /Learn
1. Inventory existing training programs to establish a SSoT
1. Update https://about.gitlab.com/learn/ to capture SSoT and provide more clarity on which training is for what

### Create future coordination processes and plan
1. Clearly identify how key teams will work together in future to ensure optimal training experience and minimal inefficiencies.


## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Sid Sijbandij | CEO |
| Facilitator           | Stella Treas            | Chief of Staff      |
| Functional Lead       | Kris Reynolds      | Manager, Field Enablement Programs  |
| Functional Lead       | Josh Zimmerman          | Manager, Learning and Development   |
| Functional Lead       | Tye Davis         | Manager, Technical Marketing   |
| Functional Lead       | Ed Cepulis         | Sr. Director, Channel Programs & Enablement   |
| Functional Lead       | Michael Lutz         | Sr. Director, Professional Services   |
| Functional Lead       | Christina Hupy         | Manager, Education Program   |
| Member                | Samantha Lee           | Learning and Development Generalist        |
| Member                | Thabo Bopape            | Senior Technical Enablement Program Manager      |
| Member                | Kendra Marquat     | Sr. Technical Instructional Designer, Customer Education |
| Member                | Christopher Wang      | Senior Sales Development Solutions Architect        |
| Member                | Marshall Cottrell      | Strategy and Operations Principal        |


## Meetings

Meetings notes are available to all team members: https://docs.google.com/document/d/1P-TEi3yXtRum2pV8KDHON4EIn-mYoH72YpDMJGClFR4/edit#heading=h.3f77vfk6c3o1. 

