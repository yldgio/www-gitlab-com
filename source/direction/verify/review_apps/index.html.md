---
layout: markdown_page
title: "Category Direction - Review Apps"
description: Review Apps let you build a review process right into your software development workflow by automatically provisioning test environments for your code. 
canonical_path: "/direction/verify/review_apps/"
---
 
- TOC
{:toc}
 
## Review Apps
 
Review Apps let you build a review process right into your software development workflow by automatically provisioning test environments for your code, integrated right into your merge requests. Not only can you enable your teammates to easily participate in the review, but you also can also shift additional activities left, such as running [DAST](https://about.gitlab.com/direction/secure/dynamic-analysis/dast/) in your review apps.
 
This ties into our [progressive delivery](https://about.gitlab.com/direction/ops/#progressive-delivery) vision of CI/CD as it gives you a glipse of how  your application will look after a specific commit, way before it reaches production.
Our ultimate goal is that Review Apps should spin up with a one-click button that works automatically regardless of the deployment target (this includes cloud-native and mobile as well). Anyone can view, comment and even fix any errors found directly from the review app itself.
 
![Review Apps]( /images/direction/cicd/review-apps.png) 
 
This area of the product is in need of continued refinement to add more kinds of
review apps (such as for mobile devices), and a smoother, easier to use experience.
 
- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AReview%20Apps)
- [Overall Vision](/direction/ops/#release)
- [Documentation](https://docs.gitlab.com/ee/ci/review_apps/)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3AReview%20Apps) 
- [Research insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3AReview%20Apps)
 
## What's Next & Why

The Release group is not actively working on Review Apps. We will work on bug fixes and make some UX improvements. If there are specific issues that you are interested in, please upvote the issue and/or leave feedback on the issue itself. 

## Maturity Plan
 
This category is currently at the "Complete" maturity level, and our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/#maturity-plan)).

The next set of features we are considering are captured in the [Review Apps: Lovable Maturity Epic](https://gitlab.com/groups/gitlab-org/-/epics/2251)
 
## Competitive Landscape
 
### Heroku

One big advantage Heroku Review Apps have over ours is that they are easier to set up
and get running. Ours require a bit more knowledge and reading of documentation to make
this clear. We can make our Review Apps much easier (and thereby much more visible) by
implementing [One button to enable review apps, auto-edit `.gitlab-ci.yml`, auto-configure GKE](https://gitlab.com/groups/gitlab-org/-/epics/2349),
which does the heavy lifting of getting them working for you.

### Netlify Deploy Previews

Netlify Deploy Previews provide functionality similar to Review Apps and work with both GitLab and GitHub. Combined with their notifications capabilities this provides a compelling way to share a preview of changes with intersted stakeholders as they are happing. Netlify also provides the ability to let any user leave comments on these deploy previews as noted on the [Usability Direction Page](/direction/verify/usability_testing/#netlify-deploy-previews).
 
## Top Customer Success/Sales Issue(s)
 
Management of Review Apps can be a challenge, particularly in cleaning them up. To
highlight the severity of how this issue can grow, [www-gitlab-com project has many review apps that may be stale](https://gitlab.com/gitlab-com/www-gitlab-com/-/environments), 
with no clear easy way to clean them up. There are two main items that look to address this challenge:
 
- [gitlab#20956](https://gitlab.com/gitlab-org/gitlab/issues/20956) builds in an expiration date for review apps, beyond which they will automatically be terminated (Delivered in 12.8).
- [gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724) (also the 2nd customer issue) implements a way to clean up environments that either did not have an expiration date or were not terminated for other reasons.
 
Review Apps for GitLab Pages is a highly requested functionality [gitlab#16907](https://gitlab.com/gitlab-org/gitlab/issues/16907).
 
## Top Customer Issue(s)
 
[gitlab#235686](https://gitlab.com/gitlab-org/gitlab/-/issues/235686) extends support for review apps from a child pipeline when using [parent-child pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html).
 
## Top Vision Item(s)
 
Review Apps takes time and effort to setup and manage. Why if customers don't have to do any of the work of setup and management? We are considering offering [Review Apps as a Service](https://gitlab.com/groups/gitlab-org/-/epics/5919).
